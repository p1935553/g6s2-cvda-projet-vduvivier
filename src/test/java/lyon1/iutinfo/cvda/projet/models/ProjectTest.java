/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lyon1.iutinfo.cvda.projet.models;

import java.util.ArrayList;
import lyon1.iutinfo.cvda.projet.exceptions.*;
import org.apache.commons.codec.digest.DigestUtils;
import static org.apache.commons.codec.digest.MessageDigestAlgorithms.SHA_224;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/*
	Test class for the project class
*/
public class ProjectTest {
    
      public ProjectTest() {
      }

      @BeforeAll
      public static void setUpClass() {
      }

      @AfterAll
      public static void tearDownClass() {
      }

      @BeforeEach
      public void setUp() {
      }

      @AfterEach
      public void tearDown() {
      }

      //Creates a test project and compare the string returned by toString to what it should be
      @Test
      public void testToString() throws idException, webURLException, sshURLException, nbCommitsException {
            System.out.println("toString");
            Project p1 = new Project(11, "pr 1", "https://www.site.git", "git@ssh.git", 1);
            String expResult ="Projet id #" + p1.id + " :\n";
            expResult+="Nom : \"" + p1.name + "\"\n";
            expResult+="sshURL : \"" + p1.sshURL + "\"\n";
            expResult+="webURL : \"" + p1.webURL + "\"\n";
            expResult+="Commits : " + p1.nb_commits + "\n";
            expResult+="Membres (" + p1.lst_members.size() + ")\n";
            for(Member membre : p1.lst_members){
                  expResult+="\t#" + membre.getId() + ": " + membre.getName() + 
                          " (" + membre.getLstProjects().size() + " projets)\n";
            }
            String result = p1.toString();
            assertEquals(expResult, result);
      }

     //Creates a test to check if getId returns the right id
      @Test
      public void testGetId() throws idException, webURLException, sshURLException, nbCommitsException {
            System.out.println("getId");
            Project p1 = new Project(11, "pr 1", "https://www.site.git", "git@ssh.git", 1);
            int expResult = 11;
            int result = p1.getId();
            assertEquals(expResult, result);
      }

      //to check is the setter sets the right id
      @Test
      public void testSetId() throws idException, webURLException, sshURLException, nbCommitsException{
            System.out.println("setId");
            int id = 1;
            if(id<0){
                  throw new idException();
            }else{
                  Project p1 = new Project(11, "pr 1", "https://www.site.git", "git@ssh.git", 1);
                  p1.setId(id);
			assertEquals(id, p1.getId());
            }
      }

      //Checks if the name is correctly returned
      @Test
      public void testGetName() throws idException, webURLException, sshURLException, nbCommitsException {
            System.out.println("getName");
            Project p1 = new Project(11, "pr 1", "https://www.site.git", "git@ssh.git", 1);
            String expResult = "pr 1";
            String result = p1.getName();
            assertEquals(expResult, result);
      }

      //checks if the name is correctly set
      @Test
      public void testSetName() throws idException, webURLException, sshURLException, nbCommitsException {
            System.out.println("setNom");
            String name = "pr t";
            Project p1 = new Project(11, "pr 1", "https://www.site.git", "git@ssh.git", 1);
            p1.setName(name);
		assertEquals(name, p1.getName());
      }

      //Checks if the url of the repo is correctly returned
      @Test
      public void testGetWebURL() throws idException, webURLException, sshURLException, nbCommitsException {
            System.out.println("getWebURL");
            Project p1 = new Project(11, "pr 1", "https://www.site.git", "git@ssh.git", 1);
            String expResult = "https://www.site.git";
            String result = p1.getWebURL();
            assertEquals(expResult, result);
      }

      //Checks if the url of the repo is correctly set
      @Test
      public void testSetWebURL() throws idException, webURLException, sshURLException, nbCommitsException {
            System.out.println("setWebURL");
            String webURL = "https://azeaveaze.git";
		//to check if it is possible for the web url to contains at least "http://"
            if(webURL.length()>8){
			//check the beginning and the end of the url
                  if(webURL.substring(0, 8).equals("https://") && webURL.substring(webURL.length()-4, webURL.length()).equals(".git")){
                        Project p1 = new Project(11, "pr 1", "https://www.site.git", "git@ssh.git", 1);
                        p1.setWebURL(webURL);
				assertEquals(webURL, p1.getWebURL());
                  }else{
                        throw new webURLException();
            }
            }else{
                  throw new webURLException();
            }
      }

	//Checks if the ssh url of the repo is correctly returned
      @Test
      public void testGetSshURL() throws idException, webURLException, sshURLException, nbCommitsException {
            System.out.println("getSshURL");
            Project p1 = new Project(11, "pr 1", "https://www.site.git", "git@ssh.git", 1);
            String expResult = "git@ssh.git";
            String result = p1.getSshURL();
            assertEquals(expResult, result);
      }

      //Checks if the ssh url of the repo is correctly set
      @Test
      public void testSetSshURL() throws idException, webURLException, sshURLException, nbCommitsException {
            System.out.println("setSshURL");
            String sshURL = "git@sshtest.git";
		//to check if it is possible for the web url to contains at least "http://"
            if(sshURL.length()>4){
			//check the beginning and the end of the url
                  if(sshURL.substring(0, 4).equals("git@") && sshURL.substring(sshURL.length()-4, sshURL.length()).equals(".git")){
                        Project p1 = new Project(11, "pr 1", "https://www.site.git", "git@ssh.git", 1);
                        p1.setSshURL(sshURL);
				assertEquals(sshURL, p1.getSshURL());
                  }else{
                        throw new sshURLException();
            }
            }else{
                  throw new sshURLException();
            } 
      }

      //Check if the getNbCommits returns the right thing
      @Test
      public void testGetNbCommits() throws idException, webURLException, sshURLException, nbCommitsException {
            System.out.println("getNbCommits");
            Project p1 = new Project(11, "pr 1", "https://www.site.git", "git@ssh.git", 1);
            int expResult = 1;
            int result = p1.getNbCommits();
            assertEquals(expResult, result);
      }

      //Checks if the number of commit of the project is correctly set
      @Test
      public void testSetNbCommits() throws idException, webURLException, sshURLException, nbCommitsException {
            System.out.println("setNbCommits");
            int nbCommits = 3;
            if(nbCommits<0){
                  throw new nbCommitsException();
            }else{
                  Project p1 = new Project(11, "pr 1", "https://www.site.git", "git@ssh.git", 1);
                  p1.setNbCommits(nbCommits);
			assertEquals(nbCommits, p1.getNbCommits());
            }
      }

      //Check if the getLstMembers returns the right thing
      @Test
      public void testGetLstMembers() throws idException, webURLException, sshURLException, nbCommitsException {
            System.out.println("getLstMembres");
            Project p1 = new Project(11, "pr 1", "https://www.site.git", "git@ssh.git", 1);
            ArrayList<Member> expResult = new ArrayList<>();
            ArrayList<Member> result = p1.getLstMembers();
            assertEquals(expResult, result);
      }

      //Checks if the list of member of the project is correctly set
      @Test
      public void testSetLstMembers() throws idException, webURLException, sshURLException, nbCommitsException {
            System.out.println("setLstMembres");
            ArrayList<Member> lstMembers = new ArrayList<>();
            Project p1 = new Project(11, "pr 1", "https://www.site.git", "git@ssh.git", 1);
            p1.setLstMembers(lstMembers);
		assertEquals(lstMembers, p1.getLstMembers());
      }
	
	//Check if the getNbMembers returns the right thing
      @Test
      public void getNbMembers() throws idException, webURLException, sshURLException, nbCommitsException, websiteException {
            System.out.println("getNbMembres");
            Member m = new Member(1, "TestNom", "TestEmail", "TestWebsite");
            Project p1 = new Project(11, "pr 1", "https://www.site.git", "git@ssh.git", 1);
            p1.addMember(m);

            assertEquals(1, p1.getNbMembers());
      }

	//Checks if it adds correctly a new member
      @Test
      public void testAjoutMembre() throws idException, webURLException, sshURLException, nbCommitsException, websiteException {
            System.out.println("AjoutEtudiant");
            Member m = new Member(1, "TestNom", "TestEmail", "https://www.site.com");
            Project p1 = new Project(11, "pr 1", "https://www.site.git", "git@ssh.git", 1);
            p1.addMember(m);

            assertEquals(1, p1.getNbMembers());
      }
    
	//Checks if the member is correctly deleted
      @Test
      public void testSupprimerMembre() throws idException, webURLException, sshURLException, nbCommitsException, websiteException {
            System.out.println("AjoutEtudiant");
            Member m = new Member(1, "TestNom", "TestEmail", "https://www.site.com");
            Project p1 = new Project(11, "pr 1", "https://www.site.git", "git@ssh.git", 1);
            p1.addMember(m);
            p1.deleteMember(m);

            assertEquals(0, p1.getNbMembers());
      }
    
	//Checks if toXML returns the correct XML string by comparing the hashs
      @Test
      public void testToXML() throws idException, webURLException, sshURLException, nbCommitsException, websiteException {
            System.out.println("toXML");
            String hashXML;
            Project p1 = new Project(11, "pr 1", "https://www.site.git", "git@ssh.git", 1);
            Member m1 = new Member(1, "Test 1", "mailt1", "https://www.site.com");
            p1.addMember(m1);

            String xml="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>\n";
            xml+="<projet id=\"11\" nbcommits=\"1\">\n";
            xml+="\t<nom>pr 1</nom>\n";
            xml+="\t<webURL>https://www.site.git</webURL>\n";
            xml+="\t<sshURL>git@ssh.git</sshURL>\n";
            xml+="\t<membres>\n";
            xml+="\t\t<membre id=\"1\" role=\"Maintener\">\n";
            xml+="\t\t\t<nom>Test 1</nom>\n";
            xml+="\t\t\t<nb-projets>1</nb-projets>\n";   
            xml+="\t\t</membre>\n";
            xml+="\t</membres>\n";
            xml+="</projet>";
            hashXML = new DigestUtils(SHA_224).digestAsHex(xml);
            String result=p1.toXML();
            assertEquals(hashXML, result);

      }
    
}
