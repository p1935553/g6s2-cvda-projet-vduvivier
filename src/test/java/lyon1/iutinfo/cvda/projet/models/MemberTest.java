/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lyon1.iutinfo.cvda.projet.models;

import java.util.ArrayList;
import lyon1.iutinfo.cvda.projet.exceptions.idException;
import lyon1.iutinfo.cvda.projet.exceptions.websiteException;
import org.apache.commons.codec.digest.DigestUtils;
import static org.apache.commons.codec.digest.MessageDigestAlgorithms.SHA_224;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
 
/**
 *
 * @author romain
 */
public class MemberTest {

      public MemberTest() {
     }

     @BeforeAll
     public static void setUpClass() {
     }

     @AfterAll
     public static void tearDownClass() {
     }

     @BeforeEach
     public void setUp() {
     }

     @AfterEach
     public void tearDown() {
     }
     
	//Creates a test project and compare the string returned by toString to what it should be
	@Test
	public void testToString() throws idException, websiteException {
            System.out.println("toString");
            Member instance = new Member(1, "nom", "email", "http://site.com");
            String expResult = "id #1 :\n"
            +"Nom : \"nom\"\n"
            +"Email : \"email\"\n"
            +"Website : \"http://site.com\"\n"
            +"Projet : (0)\n";


            String result = instance.toString();
            assertEquals(expResult, result);
	}

      //Creates a test to check if getId returns the right id
      @Test
      public void testGetId() throws idException, websiteException {
            System.out.println("getId");
            Member instance = new Member(1, "nom", "email", "http://site.com");
            int expResult = 1;
            int result = instance.getId();
            assertEquals(expResult, result);
      }

      //to check is the setter sets the right id
      @Test
      public void testSetId() throws idException, websiteException {
            System.out.println("setId");
            int id = 1;
            Member instance = new Member(2, "nom", "email", "http://site.com");
            instance.setId(id);
            assertEquals(id, instance.id);
      }

      //Checks if the name is correctly returned
      @Test
      public void testGetName() throws idException, websiteException {
            System.out.println("getName");
            Member instance = new Member(1, "name", "email", "http://site.com");
            String expResult = "name";
            String result = instance.getName();
            assertEquals(expResult, result);
      }

      //checks if the name is correctly set
      @Test
      public void testSetName() throws idException, websiteException {
            System.out.println("setNom");
            String name = "";
            Member instance = new Member(1, "name", "email", "http://site.com");
            instance.setName(name);
            assertEquals(name, instance.name);
      }

      //Checks if the email of the member is correctly returned
      @Test
      public void testGetEmail() throws idException, websiteException {
            System.out.println("getEmail");
            Member instance = new Member(1, "nom", "email", "http://site.com");
            String expResult = "email";
            String result = instance.getEmail();
            assertEquals(expResult, result);
      }

      //Checks if the email of the member is correctly set
      @Test
      public void testSetEmail() throws idException, websiteException{
            System.out.println("setEmail");
            String email = "SetEmail";
            Member instance = new Member(1, "nom", "email", "http://site.com");
            instance.setEmail(email);
            assertEquals(email, instance.email);
      }

      //Checks if the website of the member is correctly returned
      @Test
      public void testGetWebsite() throws idException, websiteException{
            System.out.println("getWebsite");
            Member instance = new Member(1, "nom", "email", "http://site.com");
            String expResult = "http://site.com";
            String result = instance.getWebsite();
            assertEquals(expResult, result);
      }

      //Checks if the website of the member is correctly set
      @Test
      public void testSetWebsite() throws idException, websiteException{
            System.out.println("setWebsite");
            String website = "http://newsite.com";
            Member instance = new Member(1, "nom", "email", "http://site.com");
            instance.setWebsite(website);
            assertEquals(website, instance.website);
      }

      //Checks if the list of project the member has worked on is correctly returned
      @Test
      public void testGetLstProjects() throws idException, websiteException{
            System.out.println("getLstProjects");
            Member instance = new Member(1, "nom", "email", "http://site.com");
            ArrayList<Project> expResult = new ArrayList<Project>();
            ArrayList<Project> result = instance.getLstProjects();
            assertEquals(expResult, result);
      }

      //Checks if the list of project the member has worked on is correctly set
      @Test
      public void testSetLstProjects() throws idException, websiteException{
            System.out.println("setLstProjects");
            ArrayList<Project> lstProjets = new ArrayList<Project>();
            Member instance = new Member(1, "nom", "email", "http://site.com");
            instance.setLstProjets(lstProjets);
            assertEquals(lstProjets, instance.lst_projects);
      }

      //Checks if toXML returns the correct XML string by comparing the hashs
      @Test
      public void testToXML() throws idException, websiteException{
            System.out.println("toXML");
            Member instance = new Member(1, "name", "email", "http://site.com");
            String xml="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>\n";
            String expectedHASH ="";
            String instanceHASH="";
            xml+="<membre id=\"1\">\n";
            xml+="\t<nom>name</nom>\n";
            xml+="\t<email>email</email>\n";
            xml+="\t<website http://site.com/>\n";
            xml+="\t<nb-projets>0</nb-projets>";
            expectedHASH=new DigestUtils(SHA_224).digestAsHex(xml);
            instanceHASH = instance.toXML();
            System.out.println(instanceHASH);
            assertEquals(expectedHASH, instanceHASH);
      }

}