/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lyon1.iutinfo.cvda.projet.models;

import lyon1.iutinfo.cvda.projet.exceptions.*;
import org.apache.commons.codec.digest.DigestUtils;
import static org.apache.commons.codec.digest.MessageDigestAlgorithms.SHA_224;

/**
 * the main class of the project
 * @author Vincent Duvivier
 * @version 1.0
 */
public class CVDAProjetMain {

    /**
     * @param args the command line arguments
     * @throws lyon1.iutinfo.cvda.projet.exceptions.idException if id is a negative number
     * @throws lyon1.iutinfo.cvda.projet.exceptions.webURLException if the URL doesn't start by "https://" and doesn't end by ".git"
     * @throws lyon1.iutinfo.cvda.projet.exceptions.sshURLException if the URL doesn't start by "git@" and doesn't end by ".git"
     * @throws lyon1.iutinfo.cvda.projet.exceptions.nbCommitsException if nb_commits is a negative number
     * @throws lyon1.iutinfo.cvda.projet.exceptions.websiteException if the URL doesn't start by "http://" or "https://" and doesn't end by ".git"
     */
      public static void main(String[] args) throws idException, webURLException, sshURLException, nbCommitsException, websiteException {
            String hashXML;
            String testXML;
        
            Member t1 = new Member(1, "testNom", "test@mail.com", "https://www.site.com");
        
            Project p1 = new Project(11, "pr 1", "https://www.site.git", "git@ssh.git", 5);
        
            t1.addProject(p1);
        
            System.out.println(p1.toString());
        
            String xml="<projet id=\"11\" nbcommits=\"5\">\n";
            xml+="\t<nom>pr 1</nom>\n";
            xml+="\t<webURL>webpr1</webURL>\n";
            xml+="\t<sshURL>sshpr1</sshURL>\n";
            xml+="\t<membres>\n";
            xml+="\t\t<membre id=\"1\" role=\"Maintener\">\n";
            xml+="\t\t\t<nom>Test 1</nom>\n";
            xml+="\t\t\t<nb-projets>1</nb-projets>\n";   
            xml+="\t\t</membre>\n";
            xml+="\t</membres>\n";
            xml+="</projet>";
        
            hashXML = new DigestUtils(SHA_224).digestAsHex(xml);
            testXML=p1.toXML();
        
            if(hashXML.equals(testXML)){
                  System.out.println("OUAIS");
                  System.out.println(hashXML);
            }else{
                  System.out.println("NON");
            }
            
            testXML=t1.toXML();
            System.out.println(testXML);
    }
    
}
