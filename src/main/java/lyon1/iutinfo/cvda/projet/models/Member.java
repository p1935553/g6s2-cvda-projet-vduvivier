/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lyon1.iutinfo.cvda.projet.models;

import java.util.*;
import lyon1.iutinfo.cvda.projet.exceptions.*;
import org.apache.commons.codec.digest.DigestUtils;
import static org.apache.commons.codec.digest.MessageDigestAlgorithms.SHA_224;


/**
 * The class of a member of a project on the forge
 * @author Vincent Duvivier
 * @version 1.0
 */
public class Member {
	/**
	 * the identifier of the member
	 */
      protected int id;
	/**
	 * the name of the member
	 */
      protected String name;
	/**
	 * the email adress of the member
	 */
      protected String email;
	/**
	 * the website of the member
	 */
      protected String website;
	/**
	 * the list of project of the member
	 */
      protected ArrayList<Project> lst_projects;

	/**
	 * Constructor of a member
	 * @param i the identification number of the member
	 * @param n the name of the member
	 * @param e the email of the member
	 * @param w the webside of the member
	 * @throws idException if id is a negative number
	 * @throws websiteException if the website doesn't start by http:// or https://
	 */
      public Member(int i, String n, String e, String w) throws idException, websiteException{
            this.setId(i);
            this.setName(n);
            this.setEmail(e);
            this.setWebsite(w);
            lst_projects = new ArrayList<>();
      }
	
	/**
	 * Create an XML string of the differents informations of a person, turns it to 
	 * an hash String to compare in the test if it is the right one
	 * @return an hash of the XML created
	 */
		String toXML(){
		String xml="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>\n";
		String xmlHASH ="";
		xml+="<membre id=\""+this.getId()+"\">\n";
		xml+="\t<nom>"+this.getName()+"</nom>\n";
		xml+="\t<email>"+this.getEmail()+"</email>\n";
		xml+="\t<website "+this.getWebsite()+"/>\n";
		xml+="\t<nb-projets>"+this.getLstProjects().size()+"</nb-projets>";
		xmlHASH=new DigestUtils(SHA_224).digestAsHex(xml);

		return xmlHASH;

      }
		
	/**
	 * toString method to display information about a member
	 */
      public String toString(){
            String info="id #" + id + " :\n";
            info+="Nom : \"" + name + "\"\n";
            info+="Email : \"" + email + "\"\n";
            info+="Website : \"" + website + "\"\n";
            info+="Projet : (" + lst_projects.size() + ")\n";
            for(Project projet : lst_projects){
                  info+="\t#" + projet.getId() + ": " + projet.getName() +
                          " (" + projet.getLstMembers().size() + " membres)\n";
            }

            return info;
        }
	
      /**
	 * it returns the identification number of a member
       * @return the id of a member
       */
      public int getId() {
            return id;
      }

	/**
	 * Change the id of the member
       * @param i the id to set
       * @throws lyon1.iutinfo.cvda.projet.exceptions.idException if id is a negative number
       */
      public void setId(int i) throws idException{
            if(id<0){
                  throw new idException();
            }else{
                  this.id = i;
            }
      }

      /**
	 * it returns the name of the member
	 * @return the name 
       */
      public String getName() {
            return name;
      }

      /**
	 * Set a new name for the member
       * @param n the name to set
       */
      public void setName(String n) {
            this.name = n;
      }

      /**
	 * It returns the email of the member
       * @return the email
       */
      public String getEmail() {
            return email;
      }

      /**
	 * Set a new email to the member
       * @param email the email to set
       */
      public void setEmail(String email) {
            this.email = email;
      }

    /**
     * it return the website of the member
     * @return the website
     */
      public String getWebsite() {
            return website;
      }

      /**
	 * Change the website of a member
       * @param web the website to set
	 * @throws lyon1.iutinfo.cvda.projet.exceptions.websiteException if the URL doesn't start by "https://" or "http://" and doesn't end by ".git"
       */
      public void setWebsite(String web) throws websiteException {
            if(web.length()>4){
                  if(web.substring(0, 7).equals("http://") || web.substring(0, 8).equals("https://")){
                        this.website=web;
                  }else{
                        throw new websiteException();
                  }
            }else{
                  throw new websiteException();
            }
      }

      /**
	 * It returns the list of projects the member has worked on
       * @return the list of projects
       */
      public ArrayList<Project> getLstProjects() {
            return lst_projects;
      }

      /**
	 * Change the list of projects for a member
       * @param lstProjects the lstProjects to set
       */
      public void setLstProjets(ArrayList<Project> lstProjects) {
            this.lst_projects = lstProjects;
      }
	/**
	 * It returns the number of project a member has worked on
	 * @return the number of project
	 */
      public int getNbProjects(){
            return this.getLstProjects().size();
      }
	
	/**
	 * Add a new project to a member
	 * @param p new project
	 */
      public void addProject(Project p){
            this.getLstProjects().add(p);
            p.getLstMembers().add(this);
      }
	/**
	 * Delete a project to a member
	 * @param p the project to be deleted
	 */
      public void deleteProject(Project p){
            this.getLstProjects().remove(p);
            p.getLstMembers().remove(this);
      }
    
}
