/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lyon1.iutinfo.cvda.projet.models;

import lyon1.iutinfo.cvda.projet.exceptions.*;
import java.util.*;
import org.apache.commons.codec.digest.DigestUtils;
import static org.apache.commons.codec.digest.MessageDigestAlgorithms.SHA_224;

/**
 * The class of a project on the forge
 * @author Vincent Duvivier
 * @version 1.0
 */
public class Project {
	/**
	 * The identifier of the project
	 */
      protected int id;
	/**
	 * The project of the project
	 */
      protected String name;
	/**
	 * The web url of the git repository of the project
	 */
      protected String webURL;
	/**
	 * The ssh url of the git repository of the project
	 */
      protected String sshURL;
	/**
	 * The total number of commits 
	 */
      protected int nb_commits;
	/**
	 * The list of member working on the project
	 */
      protected ArrayList<Member> lst_members;
	/**
	 * The constructor of a project
	 * @param i identification number of the project
	 * @param n name of the project
	 * @param w the web url of the repository of the project
	 * @param s the ssh url of the repository of the project
	 * @param nb the total number of commit of the project
	 * @throws idException  if id is a negative number
	 * @throws webURLException if the URL doesn't start by "https://" and doesn't end by ".git"
	 * @throws sshURLException  if the URL doesn't start by "git@" and doesn't end by ".git"
	 * @throws nbCommitsException  if the number of commit is a negative number
	 */
      public Project(int i, String n, String w, String s, int nb) throws idException, webURLException, sshURLException, nbCommitsException{
            this.setId(i);
            this.setName(n);
            this.setWebURL(w);
            this.setSshURL(s);
            this.setNbCommits(nb);
            lst_members = new ArrayList<>();
      }
    
	/**
	 * toString method to display information about a project
	 */
      @Override
      public String toString(){
            String info="Projet id #" + id + " :\n";
            info+="Nom : \"" + name + "\"\n";
            info+="sshURL : \"" + sshURL + "\"\n";
            info+="webURL : \"" + webURL + "\"\n";
            info+="Commits : " + nb_commits + "\n";
            info+="Membres (" + lst_members.size() + ")\n";
            for(Member membre : lst_members){
                  info+="\t#" + membre.getId() + ": " + membre.getName() + 
                        " (" + membre.getLstProjects().size() + " projets)\n";
            }
        
            return info;
      }

      /**
	 * it returns the identification number of the project
       * @return the id of a project
       */
      public int getId() {
            return id;
      }

      /**
	 * Change the id of the project
       * @param i the id to set
       * @throws lyon1.iutinfo.cvda.projet.exceptions.idException  if id is a negative number
       */
      public void setId(int i) throws idException{
            if(id<0){
                  throw new idException();
            }else{
                  this.id = i;
            }
      }

      /**
	 * it returns the name of the project
	 * @return the name 
       */
      public String getName() {
            return name;
      }

      /**
	 * Set a new name for the project
       * @param n the name to set
       */
      public void setName(String n) {
            this.name = n;
      }

      /**
	 * return the web url on the forge of the project
	 * @return the url of the project
       */
      public String getWebURL() {
            return webURL;
      }

      /**
	 * Set a new ssh url for the project
       * @param web the web URL of the .git of the project
       * @throws lyon1.iutinfo.cvda.projet.exceptions.webURLException  if the URL doesn't start by "https://" and doesn't end by ".git"
       */
      public void setWebURL(String web) throws webURLException{ 
            if(web.length()>8){
                  if(web.substring(0, 8).equals("https://") && web.substring(web.length()-4, web.length()).equals(".git")){
                  this.webURL = web;
                  }else{
                        throw new webURLException();
                  }
            }else{
                  throw new webURLException();
            }
      }
    

      /**
	 * It returns the ssh url on the forge of the project
	 * @return the url of the ssh of the project
       */
      public String getSshURL() {
            return sshURL;
      }

      /**
	 * Set a new url for the ssh of the project
       * @param ssh the ssh URL of the .git of the project
       * @throws lyon1.iutinfo.cvda.projet.exceptions.sshURLException if the URL doesn't start by "git@" and doesn't end by ".git"
       */
      public void setSshURL(String ssh) throws sshURLException {
            if(ssh.length()>4){
                  if(ssh.substring(0, 4).equals("git@") && ssh.substring(ssh.length()-4, ssh.length()).equals(".git")){
                        this.sshURL = ssh;
                  }else{
                        throw new sshURLException();
                  }
            }else{
                  throw new sshURLException();
            }
      }

      /**
	 * It returns the total number of commits of a project
       * @return the number of commits on the project
       */
      public int getNbCommits() {
            return nb_commits;
      }

      /**
	 * Set a new number of total commit for the project
       * @param nb number of commits
       * @throws lyon1.iutinfo.cvda.projet.exceptions.nbCommitsException if nb_commits is a negative number
       */
      public void setNbCommits(int nb) throws nbCommitsException{
            if(nb_commits<0){
                  throw new nbCommitsException();
            }else{
                  this.nb_commits = nb;
            }  
      }

      /**
	 * It returns an Arraylist of all the members of a project
       * @return the list of the different members working of the project
       */
      public ArrayList<Member> getLstMembers() {
            return lst_members;
      }

      /**
	 * Set a new list of member for the project
       * @param lst_members the list of members to set
       */
      public void setLstMembers(ArrayList<Member> lst_members) {
            this.lst_members = lst_members;
      }
	/**
	 * It returns the number of members that are working on the project
	 * @return the number of members 
	 */
      public int getNbMembers(){
            return this.getLstMembers().size();
      }
	/**
	 * Add a new member to the project
	 * @param m new member
	 */
      public void addMember(Member m){
            this.getLstMembers().add(m);
            m.getLstProjects().add(this);
      }
	/**
	 * Delete a member from the project
	 * @param m the member that will be deletes
	 */
      public void deleteMember(Member m){
            this.getLstMembers().remove(m);
            m.getLstProjects().remove(this);
      }
	/**
	 * Create an XML string of the differents informations of the project, turns it to 
	 * an hash String to compare in the test if it is the right one
	 * @return an hash of the XML created
	 */
      public String toXML(){
            String xml="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>\n";
            String hashXML;
        
            xml+="<projet id=\""+this.id+"\" nbcommits=\""+this.nb_commits+"\">\n";
            xml+="\t<nom>"+this.name+"</nom>\n";
            xml+="\t<webURL>"+this.webURL+"</webURL>\n";
            xml+="\t<sshURL>"+this.sshURL+"</sshURL>\n";
            xml+="\t<membres>\n";
            for(Member m : this.lst_members){
                  xml+="\t\t<membre id=\""+m.getId()+"\" role=\"Maintener\">\n";
                  xml+="\t\t\t<nom>"+m.getName()+"</nom>\n";
                  xml+="\t\t\t<nb-projets>"+m.getLstProjects().size()+"</nb-projets>\n";   
            }
            xml+="\t\t</membre>\n";
            xml+="\t</membres>\n";
            xml+="</projet>";
            hashXML = new DigestUtils(SHA_224).digestAsHex(xml);
            return hashXML;
      }
}
