/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lyon1.iutinfo.cvda.projet.exceptions;

/**
 * Exception if the URL doesn't start by "https://" and doesn't end by ".git"
 * @author Vincent Duvivier
 * @version 1.0
 */
public class webURLException extends Exception{
	/**
	 * if the URL doesn't start by "https://" and doesn't end by ".git"
	 */
    public webURLException(){
        super("webURL doit commencer par \"https\" et finir par \".git\"");
    }
}
