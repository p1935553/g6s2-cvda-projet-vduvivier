/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lyon1.iutinfo.cvda.projet.exceptions;

/**
 * Exception if the URL doesn't start by "git@" and doesn't end by ".git"
 * @author Vincent Duvivier
 * @version 1.0
 */
public class sshURLException extends Exception{
	/**
	 * if the URL doesn't start by "git@" and doesn't end by ".git"
	 */
    public sshURLException(){
        super("sshURL doit commencer par \"git@\" et finir par \".git\"");
    }
}
