/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lyon1.iutinfo.cvda.projet.exceptions;

/**
 * Exception if the number of commit is a negative number
 * @author Vincent Duvivier
 * @version 1.0
 */
public class nbCommitsException extends Exception{
	/**
	 * if the number of commit is a negative number
	 */
	public nbCommitsException(){
		super("nbCommit doit être positif ou nul");
    }
}
