# g6s2-cvda-projet-vduvivier

DUVIVIER Vincent

mail : `vincent.duvivier@etu.univ-lyon1.fr`

Débutant total, je ne connaissais rien avant de démarrer les cours

Binôme :

DUPRE Romain

mail : `romain.dupre-l'hotel@etu.univ-lyon1.fr`

https://forge.univ-lyon1.fr/p1806446/g6s2-cvda-projet-rdupre


Kanboard du projet : https://g6s2-2020-iut-lyon1.framaboard.org/kanboard/?controller=BoardViewController&action=show&project_id=17&search=status%3Aopen

Documentation Kanboard : https://docs.kanboard.org/fr/latest/

http://yuml.me/edit/a08e5bc8

![alt text](http://yuml.me/a08e5bc8.png)

http://yuml.me/a08e5bc8.svg

```
(start)-|a|
|a|->(Connexion a la forge)-><a>
<a>[Connexion reussie]->(Recuperation des projets)-><b>
<a>[connexion echouee]->(Message erreur acces forge)->(end)
<b>[projets existant]->(traduction)-><c>
<b>[pas de projet]->(message erreur pas de projet)->(end)
<c>->(choix XML)->|d|
<c>->(choix JSON)->|d|
<c>->(choix HTML)->|d|
<c>->(choix PDF)->|d|
<c>-(Autre format)->(Message erreur format)->(end)
|d|->(end)
```


HASH essai XML : df5b5b5d5e802bbfdcb7ad4214378a75687c78db9d8f6077512621bf

La javadoc se trouve dans le repertoire target/site/apidocs/index.html

